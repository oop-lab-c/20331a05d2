import java.util.*;
interface Abs {
    void show();

}

class PureAbsJava implements Abs {
    public void show() {
        System.out.println("welcome to java");
    }

    public static void main(String[] args) {
        PureAbsJava b = new PureAbsJava();
        b.show();

    }

}

