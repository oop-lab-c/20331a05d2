#include<iostream>
using namespace std;
class Student
{
    public:
        Student(string fullName,int rollNo,float semPercentage,string collegeName,int collegeCode)//student constructor 
        {
            cout<<"\tYOUR DETAILS\t"<<endl;
            cout<<"Your name is: "<<fullName<<endl;
            cout<<"Your rollnumber is: "<<rollNo<<endl;
            cout<<"Your study percentage is: "<<semPercentage<<endl;
            cout<<"Your college name is: "<<collegeName<<endl;
            cout<<"Your college code is: "<<collegeCode<<endl;
        }~Student()//distructor syntax is ~constructor_name(){ some statements }
        {
           cout<<"Constructor is Destructed"<<endl; 
        }
};
int main()
{
    cout<<"Enter your name:";
    string name;
    cin>>name;
    cout<<"Enter your roll-number: ";
    int n;
    cin>>n;
    cout<<"Enter your percentage: ";
    float f;
    cin>>f;
    cout<<"Enter your college name: ";
    string collegename;
    cin>>collegename;
    cout<<"Enter your college code: ";
    int code;
    cin>>code;
    Student obj(name,n,f,collegename,code);//calling a constructor by passing parameters
    return 0;

}
