#include<iostream>
using namespace std;
class parent1
{
    public:
    void displayp1()
    {
        cout<<"Hello parent1 !"<<endl;
    }
};
class parent2
{
    public:
    void displayp2()
    {
        cout<<"Hello parent2 !"<<endl;
    }
};
class child1:public parent1{
    public:
    void displayc1()
    {
        cout<<"Hello child1 !"<<endl;
    }
};
class child2:public parent1,public parent2{
    public:
    void displayc2()
    {
        cout<<"Hello child2 !"<<endl;
    }
};
class child3:public child1{
    public:
    void displayc3(){
        cout<<"Hello child3 !"<<endl;
    }
};
class child4:public parent2{
    public:
    void displayc4()
    {
        cout<<"Hello child4 !"<<endl;
    }
};
class child5:public parent2{
    public:
    void displayc5()
    {
        cout<<"Hello child1 !"<<endl;
    }
};
int main()
{
    child1 objc1;
    child2 objc2;
    child3 objc3;
    child4 objc4;
    child5 objc5;
    cout<<"Simple inheritance "<<endl;
    objc1.displayp1();
    cout<<"\n multiple inheritance "<<endl;
    objc2.displayp1();
    objc2.displayc2();
    objc2.displayp2();
    cout<<"\n multilevel inheritance "<<endl;
    objc3.displayc1();
    objc3.displayc3();
    cout<<"\n hierarchical inheritance "<<endl;
    objc4.displayc4();
    objc4.displayp2();
    objc5.displayc5();
    objc5.displayp2();
    return 0;
}