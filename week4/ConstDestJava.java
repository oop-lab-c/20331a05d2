import java.util.Scanner;
public class ConstDestJava
{
    String fullName;
    int rollNo;
    double semPercentage;
    String collegeName;
    int collegeCode;
    ConstDestJava(String fn,int rn,double sp,String cn,int cc)
    {
        fullName= fn;
        rollNo=rn;
        semPercentage=sp;
        collegeName=cn;
        collegeCode=cc;
    }
    void display()
    {
        System.out.println(fullName+" "+rollNo+" "+semPercentage+" "+collegeName+" "+collegeCode);
    }
    public static void main(String[] args)
    {
        ConstDestJava obj = new ConstDestJava("nimmi",1,90.00,"mvgr",33);
        obj.display();
    }
}